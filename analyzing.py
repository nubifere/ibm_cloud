#!/usr/bin/env python
# coding: utf-8

# Analyzing a dataset

# ## Selected Socioeconomic Indicators in Chicago
# 
# The city of Chicago released a dataset of socioeconomic data to the Chicago City Portal.
# This dataset contains a selection of six socioeconomic indicators of public health significance and a “hardship index” for each Chicago community area, for the years 2008 – 2012.
# 
# Scores on the hardship index can range from 1 to 100, with a higher index number representing a greater level of hardship.
# 
# A detailed description of the dataset can be found on [the city of Chicago's website](
# https://data.cityofchicago.org/Health-Human-Services/Census-Data-Selected-socioeconomic-indicators-in-C/kn9c-c2s2), but to summarize, the dataset has the following variables:
# 
# * **Community Area Number** (`ca`): Used to uniquely identify each row of the dataset
# 
# * **Community Area Name** (`community_area_name`): The name of the region in the city of Chicago 
# 
# * **Percent of Housing Crowded** (`percent_of_housing_crowded`): Percent of occupied housing units with more than one person per room
# 
# * **Percent Households Below Poverty** (`percent_households_below_poverty`): Percent of households living below the federal poverty line
# 
# * **Percent Aged 16+ Unemployed** (`percent_aged_16_unemployed`): Percent of persons over the age of 16 years that are unemployed
# 
# * **Percent Aged 25+ without High School Diploma** (`percent_aged_25_without_high_school_diploma`): Percent of persons over the age of 25 years without a high school education
# 
# * **Percent Aged Under** 18 or Over 64:Percent of population under 18 or over 64 years of age (`percent_aged_under_18_or_over_64`): (ie. dependents)
# 
# * **Per Capita Income** (`per_capita_income_`): Community Area per capita income is estimated as the sum of tract-level aggragate incomes divided by the total population
# 
# * **Hardship Index** (`hardship_index`): Score that incorporates each of the six selected socioeconomic indicators
# 
# We'll take a look at the variables in the socioeconomic indicators dataset and do some basic analysis.
# 

# Let us first load the SQL extension and establish a connection with the database

# In[1]:


get_ipython().run_line_magic('load_ext', 'sql')


# In[2]:


get_ipython().run_line_magic('sql', 'ibm_db_sa://pkj39892:xfhbc1h8q%5Efdv71t@dashdb-txn-sbox-yp-lon02-02.services.eu-gb.bluemix.net:50000/BLUDB')


# In[3]:


import pandas
chicago_socioeconomic_data = pandas.read_csv('https://data.cityofchicago.org/resource/jcxq-k9xf.csv')
get_ipython().run_line_magic('sql', 'PERSIST chicago_socioeconomic_data')


# In[4]:


get_ipython().run_line_magic('sql', 'SELECT * FROM chicago_socioeconomic_data limit 5;')


# In[5]:


get_ipython().run_line_magic('sql', 'SELECT COUNT(*) FROM chicago_socioeconomic_data;')


# ##### How many community areas in Chicago have a hardship index greater than 50.0?

# In[6]:


get_ipython().run_line_magic('sql', 'SELECT COUNT(*) FROM chicago_socioeconomic_data WHERE hardship_index > 50.0;')


# ##### What is the maximum value of hardship index in this dataset?

# In[7]:


get_ipython().run_line_magic('sql', 'SELECT MAX(hardship_index) FROM chicago_socioeconomic_data;')


# ##### Which community area which has the highest hardship index?

# In[8]:


get_ipython().run_line_magic('sql', 'SELECT community_area_name FROM chicago_socioeconomic_data where hardship_index=98.0')


# ##### Which Chicago community areas have per-capita incomes greater than $60,000?

# In[9]:


get_ipython().run_line_magic('sql', 'SELECT community_area_name FROM chicago_socioeconomic_data WHERE per_capita_income_ > 60000;')


# ##### Let's create a scatter plot using the variables `per_capita_income_` and `hardship_index`. 

# In[10]:


import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')
import seaborn as sns

income_vs_hardship = get_ipython().run_line_magic('sql', 'SELECT per_capita_income_, hardship_index FROM chicago_socioeconomic_data;')
plot = sns.jointplot(x='per_capita_income_',y='hardship_index', data=income_vs_hardship.DataFrame())

