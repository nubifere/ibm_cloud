#!/usr/bin/env python
# coding: utf-8

# American safety: Chicago

# We'll be using three datasets that are available on the city of Chicago's Data Portal:
# 1. <a>Socioeconomic Indicators in Chicago</a>
# 1. <a>Chicago Public Schools</a>
# 1. <a>Chicago Crime Data</a>
# 
# ### 1. Socioeconomic Indicators in Chicago
# This dataset contains a selection of six socioeconomic indicators of public health significance and a “hardship index,” for each Chicago community area, for the years 2008 – 2012.
# 
# A snapshot of this dataset which can be downloaded from:
# https://ibm.box.com/shared/static/05c3415cbfbtfnr2fx4atenb2sd361ze.csv
# 
# A detailed description of this dataset and the original dataset can be obtained from the Chicago Data Portal at:
# https://data.cityofchicago.org/Health-Human-Services/Census-Data-Selected-socioeconomic-indicators-in-C/kn9c-c2s2
# 
# 
# 
# ### 2. Chicago Public Schools
# 
# This dataset shows all school level performance data used to create CPS School Report Cards for the 2011-2012 school year. This dataset is provided by the city of Chicago's Data Portal.
# 
# A snapshot of this dataset which can be downloaded from:
# https://ibm.box.com/shared/static/f9gjvj1gjmxxzycdhplzt01qtz0s7ew7.csv
# 
# A detailed description of this dataset and the original dataset can be obtained from the Chicago Data Portal at:
# https://data.cityofchicago.org/Education/Chicago-Public-Schools-Progress-Report-Cards-2011-/9xs2-f89t
# 
# 
# 
# 
# ### 3. Chicago Crime Data 
# 
# This dataset reflects reported incidents of crime (with the exception of murders where data exists for each victim) that occurred in the City of Chicago from 2001 to present, minus the most recent seven days. 
# 
# This dataset is quite large - over 1.5GB in size with over 6.5 million rows. For the purposes of this assignment we will use a much smaller sample of this dataset which can be downloaded from:
# https://ibm.box.com/shared/static/svflyugsr9zbqy5bmowgswqemfpm1x7f.csv
# 
# A detailed description of this dataset and the original dataset can be obtained from the Chicago Data Portal at:
# https://data.cityofchicago.org/Public-Safety/Crimes-2001-to-present/ijzp-q8t2
# 

# ### Download the datasets
# 
# 1. __CENSUS_DATA:__ https://ibm.box.com/shared/static/05c3415cbfbtfnr2fx4atenb2sd361ze.csv
# 1. __CHICAGO_PUBLIC_SCHOOLS__  https://ibm.box.com/shared/static/f9gjvj1gjmxxzycdhplzt01qtz0s7ew7.csv
# 1. __CHICAGO_CRIME_DATA:__ https://ibm.box.com/shared/static/svflyugsr9zbqy5bmowgswqemfpm1x7f.csv

# Let us first load the SQL extension and establish a connection with the database

# In[1]:


get_ipython().magic(u'load_ext sql')


# In[2]:


get_ipython().magic(u'sql ibm_db_sa://pkj39892:xfhbc1h8q%5Efdv71t@dashdb-txn-sbox-yp-lon02-02.services.eu-gb.bluemix.net:50000/BLUDB')


# ##### Total number of crimes recorded in the CRIME table:

# In[3]:


# Rows in Crime table
get_ipython().magic(u'sql SELECT COUNT(*) FROM chicago_crime_data;')


# In[4]:


get_ipython().magic(u'sql SELECT * FROM chicago_crime_data limit 10;')


# ##### How many crimes involve an arrest?

# In[5]:


get_ipython().magic(u'sql SELECT COUNT(*) FROM chicago_crime_data where "ARREST" = \'TRUE\'')


# ##### Which unique types of crimes have been recorded at GAS STATION locations?

# In[6]:


get_ipython().magic(u'sql SELECT DISTINCT primary_type FROM chicago_crime_data where "LOCATION_DESCRIPTION" = \'GAS STATION\'')


# ##### In the CENSUS_DATA table list all Community Areas whose names start with the letter ‘B’.

# In[7]:


get_ipython().magic(u'sql SELECT community_area_name FROM census_data where "COMMUNITY_AREA_NAME" LIKE \'B%\'')


# ##### Which schools in Community Areas 10 to 15 are healthy school certified?

# In[8]:


get_ipython().magic(u'sql SELECT name_of_school FROM chicago_public_schools where "HEALTHY_SCHOOL_CERTIFIED" = \'Yes\' and "COMMUNITY_AREA_NUMBER" between 10 and 15')


# ##### What is the average school Safety Score? 

# In[9]:


get_ipython().magic(u'sql SELECT AVG(safety_score) as avg_safety_score from chicago_public_schools')


# ##### List the top 5 Community Areas by average College Enrollment [number of students] 

# In[10]:


get_ipython().magic(u'sql SELECT community_area_name, AVG(college_enrollment) as average_enrollment    FROM chicago_public_schools    group by community_area_name    order by average_enrollment desc    fetch first 5 rows only')


# ##### Use a sub-query to determine which Community Area has the least value for school Safety Score

# In[11]:


get_ipython().magic(u'sql SELECT community_area_name, safety_score FROM chicago_public_schools where   safety_score= (select MIN(safety_score) FROM chicago_public_schools)')


# ##### Per Capita Income of the Community Area which has a school Safety Score of 1

# In[12]:


get_ipython().magic(u'sql SELECT per_capita_income FROM census_data WHERE community_area_number IN (SELECT community_area_number FROM chicago_public_schools WHERE safety_score = 1)')

